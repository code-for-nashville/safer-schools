---
title: "MNPS Cluster Plotting"
author: Vienna Thompkins
output: html_notebook
---

# Setup

```{r, message = FALSE, warning = FALSE}
# load packages 
## some of these can probably be dropped once we settle on a plan for mapping
library(tidyr)
library(dplyr)
library(sf)
library(ggplot2)
library(rgeos)
library(mapview)
library(leaflet)
library(broom)
library(rgdal)
library(raster)
library(tmap)
library(usmap)
library(leafem)
library(stringr)
library(s2)
library(readr)
library(wesanderson)
library(RColorBrewer)
library(png)
library(leafpop)

# load data
## make sure that you have all of the files in the same folder or mapping will break
shp <- readOGR("Cluster Shapefiles\\MNPS_clusters_2021_coderequest.shp")
```

# Load School Data

```{r, echo = TRUE}
# load in school data csv
sch <- read.csv("SSN_Discipline.csv")

# reclass variables to prep for analysis
sch <- sch %>%
  mutate(all_pop = as.numeric(all_pop),
         grad_rate = parse_number(grad_rate)/100)
```


# Add Variables

```{r}
sch <- sch %>%
         # demo percents - what percent of all students does each demo represent?
  mutate(white_percent = white_pop/all_pop,
         black_percent = black_pop/all_pop,
         hisp_percent = hisp_pop/all_pop,
         econ_percent = econ_pop/all_pop,
         disab_percent = disab_pop/all_pop,
         # discipline proportion - what proportion of all X discipline does each demo represent?
         white_exp_prop = white_exp_count/all_exp_count,
         white_oss_prop = white_oss_count/all_oss_count,
         white_iss_prop = white_iss_count/all_iss_count,
         black_exp_prop = black_exp_count/all_exp_count,
         black_oss_prop = black_oss_count/all_oss_count,
         black_iss_prop = black_iss_count/all_iss_count,
         hisp_exp_prop = hisp_exp_count/all_exp_count,
         hisp_oss_prop = hisp_oss_count/all_oss_count,
         hisp_iss_prop = hisp_iss_count/all_iss_count,
         econ_exp_prop = econ_exp_count/all_exp_count,
         econ_oss_prop = econ_oss_count/all_oss_count,
         econ_iss_prop = econ_iss_count/all_iss_count,
         disab_exp_prop = disab_exp_count/all_exp_count,
         disab_oss_prop = disab_oss_count/all_oss_count,
         disab_iss_prop = disab_iss_count/all_iss_count,
         # law enforcement and school arrest proportions 
         white_LE_prop = white_LE/all_LE,
         white_arrest_prop = white_arrest/all_arrest,
         black_LE_prop = black_LE/all_LE,
         black_arrest_prop = black_arrest/all_arrest,
         hisp_LE_prop = hisp_LE/all_LE,
         hisp_arrest_prop = hisp_arrest/all_arrest,
         econ_LE_prop = econ_LE/all_LE,
         econ_arrest_prop = econ_arrest/all_arrest,
         disab_LE_prop = disab_LE/all_LE,
         disab_arrest_prop = disab_arrest/all_arrest)
```

# Reshape and Run Calculations

```{r}
# group by cluster
sch_clus <- sch %>%
  group_by(clus_name, CODE) %>%
            # totals and proportions
  summarise(total_pop = sum(all_pop),
            white_total_pop = sum(white_pop),
            white_prop = white_total_pop/total_pop,
            black_total_pop = sum(black_pop),
            black_prop = black_total_pop/total_pop,
            hisp_total_pop = sum(hisp_pop),
            hisp_prop = hisp_total_pop/total_pop,
            econ_total_pop = sum(econ_pop),
            econ_prop = econ_total_pop/total_pop,
            disab_total_pop = sum(disab_pop),
            disab_prop = disab_total_pop/total_pop,
            # professional staff ratios
            teacher_ratio_avg = mean(na.omit(teacher_ratio)),
            counselor_ratio_avg = mean(na.omit(counselor_ratio)),
            sro_ratio_avg = mean(na.omit(sro_ratio)),
            # expulsion total and proportions
            total_exp = sum(all_exp_count),
            white_exp_prop = sum(na.omit(white_exp_count))/total_exp,
            black_exp_prop = sum(black_exp_count)/total_exp,
            hisp_exp_prop = sum(na.omit(hisp_exp_count))/total_exp,
            econ_exp_prop = sum(econ_exp_count)/total_exp,
            disab_exp_prop = sum(disab_exp_count)/total_exp,
            # out of school suspension total and proportions
            total_oss = sum(all_oss_count),
            white_oss_prop = sum(na.omit(white_oss_count))/total_oss,
            black_oss_prop = sum(black_oss_count)/total_oss,
            hisp_oss_prop = sum(na.omit(hisp_oss_count))/total_oss,
            econ_oss_prop = sum(econ_oss_count)/total_oss,
            disab_oss_prop = sum(disab_oss_count)/total_oss,
            # in school suspension total and proportions
            total_iss = sum(all_iss_count),
            white_iss_prop = sum(na.omit(white_iss_count))/total_iss,
            black_iss_prop = sum(black_iss_count)/total_iss,
            hisp_iss_prop = sum(na.omit(hisp_iss_count))/total_iss,
            econ_iss_prop = sum(econ_iss_count)/total_iss,
            disab_iss_prop = sum(disab_iss_count)/total_iss,
            # referrals to law enforcement
            total_LE = sum(all_LE),
            white_LE_prop = sum(white_LE)/total_LE,
            black_LE_prop = sum(black_LE)/total_LE,
            hisp_LE_prop = sum(hisp_LE)/total_LE,
            disab_LE_prop = sum(disab_LE)/total_LE,
            # school arrests
            total_arrest = sum(all_arrest),
            white_arrest_prop = sum(white_arrest)/total_arrest,
            black_arrest_prop = sum(black_arrest)/total_arrest,
            hisp_arrest_prop = sum(hisp_arrest)/total_arrest,
            disab_arrest_prop = sum(disab_arrest)/total_arrest)
```

# Plot and Analyze

## Prep

```{r}
# reshape long for plotting
clus_long <- sch_clus %>%
  dplyr::select(clus_name, CODE,
                white_prop, black_prop, hisp_prop, econ_prop, disab_prop, 
                white_exp_prop, black_exp_prop, hisp_exp_prop, econ_exp_prop, disab_exp_prop,
                white_oss_prop, black_oss_prop, hisp_oss_prop, econ_oss_prop, disab_oss_prop,
                white_iss_prop, black_iss_prop, hisp_iss_prop, econ_iss_prop, disab_iss_prop,
                white_LE_prop, black_LE_prop, hisp_LE_prop, disab_LE_prop,
                white_arrest_prop, black_arrest_prop, hisp_arrest_prop, disab_arrest_prop) %>%
  pivot_longer(
    cols = ends_with("_prop"),
    names_to = "demo",
    values_to = "proportion") %>%
  # remove "_prop" from demo 
  mutate(demo = gsub("_prop", "", demo)) %>%
  # split demo into demo and group
  separate(demo, into = c("demo", "group"), sep = "_") %>%
  # convert NA values to "total"
  mutate(group = if_else(is.na(group), "total", group)) %>%
  # convert categorical variables to factors
  mutate_at(.vars = c("demo", "group"), .funs = as.factor)

# change group factor level order
clus_long$group <- ordered(clus_long$group, levels = c("total", "iss", "oss", "exp", "LE", "arrest"))

# remove extra whitespace from cluster names
clus_long$clus_name <- trimws(clus_long$clus_name)
```

# Plot

## Graph Function

```{r}
# write function using graph template
graph_fun <- function(data, cluster, demographics, discipline) {
  
  # call font to use when plotting
  windowsFonts(calibri = windowsFont("calibri"))
  
  # create table 
  out <- data %>%
    # filter to cluster and demographics, and discipline type
    dplyr::filter(clus_name == cluster,
              demo %in% demographics,
              group %in% discipline) %>%
                # set aesthetics
                # x-axis is demographic groups
    ggplot(aes(x = demo, 
                # y-axis is proportion multiplied by 100 to represent percentage
               y = proportion*100, 
                # set group to population group to prep for clustered columns
               group = group, 
                # set color fill to match by group
               fill = group,
                # set max y value 
               ymax = 100)) +
                # cluster column
      geom_col(position = position_dodge2(width = 1,
                                          preserve = "single", 
                                          padding = 0.1)) +
                # add data labels, match formatting with y-axis
      geom_text(aes(label = paste0(round(proportion*100, 0),"%")),
                # center vertically
                vjust = -0.5,
                # set position to match columns
                position = position_dodge2(width = 1, 
                                           preserve = "single", 
                                           padding = 0.1),
                # set font, because some reason it doesn't stick from theme below
                family = "calibri") +
                # apply theme
      theme_minimal(base_size = 16, base_family = "calibri") +
      theme(plot.margin = unit(c(1, 1, 1.5, 1.2), "points")) +
                # specify color palette and adjust legend
      scale_fill_manual(values = wes_palette(n = 4, 
                                             name = "Darjeeling2"),
                        name = element_blank(),
                        labels = disc_labs) +
                # replace data values with stored formatted labels
      scale_x_discrete(labels = demo_labs) +
                # format title and subtitle
      labs(title = "Discipline Disparities",
           subtitle = paste("Metro Nashville Public Schools\n2017-2018 School Year,", cluster, "Cluster")) +
                # edit axis titles
      ylab("Percent of Students") +
      xlab("Demographic Group")
  
  print(out)
}
```

```{r}
## prep to run function

# save names for labels
demo_labs <- c(white = "White\nStudents", black = "Black\nStudents", hisp = "Hispanic or Latino\nStudents", econ = "Economically\nDisadvantaged Students", disab = "Students with\nDisabilities")

disc_labs <- c(total = "\nAll Students\n", exp = "\nExpulsions\n", oss = "\nOut of School\nSuspensions\n", iss = "\nIn School\nSuspensions\n", LE = "\nReferrals to\nLaw Enforcement\n", arrest = "\nArrests\n")


## create groups for types of demos and discipline for filtering graphs

# demos
race_demos <- c("white", "black", "hisp")
other_demos <- c("econ", "disab")

# discipline & LE contact
school_discipline <- c("total", "iss", "oss", "exp")
le_arrest <- c("total", "LE", "arrest")

# store cluster names
clusters <- clus_long %>% pull(clus_name)
clusters <- unique(clusters)

# run graphs
for(i in clusters){
  
  png(paste0("plots/plot_", i, "_race_school.png"), width = 1024, height = 768)
  graph_fun(clus_long, i, race_demos, school_discipline)
  dev.off()
  
  png(paste0("plots/plot_", i, "_other_school.png"), width = 1024, height = 768)
  graph_fun(clus_long, i, other_demos, school_discipline)
  dev.off()
  
  png(paste0("plots/plot_", i, "_race_le.png"), width = 1024, height = 768)
  graph_fun(clus_long, i, race_demos, le_arrest)
  dev.off()
  
  png(paste0("plots/plot_", i, "_other_le.png"), width = 1024, height = 768)
  graph_fun(clus_long, i, "disab", le_arrest)
  dev.off()
}

```

# Mapping 

```{r}
# merge school data with shape data
dat <- sp::merge(shp, sch_clus, by = "CODE", duplicateGeoms = TRUE)

# sort data by cluster alphabetically
dat <- dat[order("CLUS_NAME"),]

# turn off spherical geometry - for some reason it's breaking mapping with these shapefiles
sf_use_s2(FALSE)

# load images for popups
imgs <- c("https://imgur.com/a/ISpXKtu", 
          "https://imgur.com/a/2K48CoV",
          "https://imgur.com/a/Ki8lrZg",
          "https://imgur.com/a/nrDfR0Y",
          "https://imgur.com/a/lvmkTxa",
          "https://imgur.com/a/nLS5fUf",
          "https://imgur.com/a/WNKBiCE",
          "https://imgur.com/a/fxnGVdp",
          "https://imgur.com/a/FoVO4Mu",
          "https://imgur.com/a/veDMCyZ",
          "https://imgur.com/a/2uCxSr2",
          "https://imgur.com/a/I6FdCYZ")

# map data
mapView(dat, 
        zcol = c("black_prop"),
        popup = popupImage(files, src = "remote", embed = TRUE)) %>%
  addStaticLabels(label = dat$clus_name)


## notes on mapview - 
# zcol adds layers to the map
# popup adds text, images, or graphs when clicking on the map (see https://environmentalinformatics-marburg.github.io/mapview/popups/html/popups.html)

```
